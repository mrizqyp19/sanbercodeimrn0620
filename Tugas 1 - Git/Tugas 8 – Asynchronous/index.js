// di index.js
var readBooks = require("./callback.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
];

var x = 0;
function waktuBaca(waktu) {
  if (x < books.length) {
    readBooks(waktu, books[x], function (sisawaktu) {
      waktuBaca(sisawaktu);
    });
  }
  x++;
}

waktuBaca(10000);

// Tulis code untuk memanggil function readBooks di sini
