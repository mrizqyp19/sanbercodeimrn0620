var readBooksPromise = require("./promise.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
];

var x = 0;
function waktuBaca(waktu) {
  if (x < books.length) {
    readBooksPromise(waktu, books[x])
      .then(function (siswaktu) {
        waktuBaca(siswaktu);
      })
      .catch(function (err) {
        console.log(err);
      });
  }
  x++;
}
waktuBaca(9000);
// Lanjutkan code untuk menjalankan function readBooksPromise
