console.log("Soal No 1");

function arrayToObject(arr) {
  if (!arr) {
    var peoples = [];
  } else {
    for (let i = 0; i < arr.length; i++) {
      var peoples = {
        firstName: arr[i][0],
        lastName: arr[i][1],
        gender: arr[i][2],
        age: menghitungUmur(arr[i][3]),
      };
      console.log(`${i + 1} - ${arr[i][0]} ${arr[i][1]} : `, peoples);
    }
  }
  return peoples;
}

function menghitungUmur(age) {
  var now = new Date();
  var thisYear = now.getFullYear();
  if (age >= 0) {
    var hasil = thisYear - age;
    if (hasil < 0) {
      return "Invalid Birth Year";
    } else {
      return hasil;
    }
  } else if (age > thisYear || !age) {
    return "Invalid Birth Year";
  }
}

var people = [
  ["Bruce", "Banner", "male", 1975],
  ["Natasha", "Romanoff", "female"],
];
arrayToObject(people);

var people2 = [
  ["Tony", "Stark", "male", 1980],
  ["Pepper", "Pots", "female", 2023],
];
arrayToObject(people2);

// Error case
arrayToObject([]); // ""

console.log("Soal No 2");
function shoppingTime(memberId, money) {
  var barangBeli = [];
  var totalBelanja = [];
  var listShoping = [
    ["Sepatu Stacattu", 1500000],
    ["Baju Zoro", 500000],
    ["Baju H&N0", 250000],
    ["Sweater Uniklooh", 175000],
    ["Casing Handphone", 50000],
  ];
  if (!memberId) {
    return "Mohon maaf, toko X hanya berlaku untuk member saja";
  }
  if (money < 50000) {
    return "Mohon maaf, uang tidak cukup ";
  }
  for (let i = 0; i < listShoping.length; i++) {
    if (money >= listShoping[i][1]) {
      barangBeli.push(listShoping[i][0]);
      totalBelanja.push(listShoping[i][1]);
    }
  }
  var fixTotal = 0;
  for (let x = 0; x < totalBelanja.length; x++) {
    fixTotal += totalBelanja[x];
  }
  var moneyMod = money - fixTotal;

  var Detail = {
    memberId: memberId,
    money: money,
    listPurchased: barangBeli,
    changeMoney: moneyMod,
  };
  return Detail;
}

// TEST CASES
console.log(shoppingTime("1820RzKrnWn08", 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime("82Ku8Ma742", 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime("", 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime("234JdhweRxa53", 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

console.log("Soal No 3");

function naikAngkot(arrPenumpang) {
  rute = ["A", "B", "C", "D", "E", "F"];
  var list = [];

  for (let i = 0; i < arrPenumpang.length; i++) {
    dari = arrPenumpang[i][1];
    tujuan = arrPenumpang[i][2];
    var indexdari = 0;
    var indextujuan = 0;

    for (let j = 0; j < rute.length; j++) {
      if (dari == rute[j]) {
        indexdari = j;
      }
      if (tujuan == rute[j]) {
        indextujuan = j;
      }

      var selisih = indextujuan - indexdari;
      totalBayar = selisih * 2000;
    }
    var fix = {};
    fix.penumpang = arrPenumpang[i][0];
    fix.naikDari = dari;
    fix.tujuan = tujuan;
    fix.bayar = totalBayar;
    list.push(fix);
  }

  return list;
}

console.log(
  naikAngkot([
    ["Dimitri", "B", "F"],
    ["Icha", "A", "B"],
  ])
);

console.log(naikAngkot([])); //[]
