console.log("LOOPING PERTAMA");
var i = 0;
while (i < 20) {
  i++;
  if (i % 2 == 0) {
    console.log(`${i} - i love coding `);
  }
}
console.log("LOOPING KEDUA");
var i = 21;
while (i > 1) {
  i--;
  if (i % 2 == 0) {
    console.log(`${i} - i love coding `);
  }
}

for (var angka = 1; angka <= 20; angka++) {
  if (angka % 2 == 0) {
    console.log(`${angka} - Santai`);
  } else if (angka % 2 == 1 && angka % 3 == 0) {
    console.log(`${angka} - i love coding`);
  } else if (angka % 2 == 1) {
    console.log(`${angka} - Berkualitas`);
  }
}

var x = 4;
var y = 8;
var s = "";
for (var i = 1; i <= x; i++) {
  for (var j = 1; j <= y; j++) {
    s += "#";
  }
  s += "\n";
}
console.log(s);

var z = "";
for (var i = 1; i <= 7; i++) {
  for (var j = 1; j <= i; j++) {
    z += "#";
  }
  z += "\n";
}
console.log(z);

var x = 8;
var y = 8;
var s = "";
for (var i = 1; i <= x; i++) {
  if (i % 2 == 0) {
    for (var j = 1; j <= y; j++) {
      if (j % 2 == 0) {
        s += " ";
      } else {
        s += "#";
      }
    }
  } else {
    for (var j = 1; j <= y; j++) {
      if (j % 2 == 0) {
        s += "#";
      } else {
        s += " ";
      }
    }
  }
  s += "\n";
}
console.log(s);
