console.log("Soal No 1\n");

function range(startNum, finishNum) {
  var fix = [];

  if (!startNum || !finishNum) {
    fix = -1;
  } else if (startNum > finishNum) {
    for (let i = startNum; i >= finishNum; i--) {
      fix.push(i);
    }
  } else {
    for (let i = startNum; i <= finishNum; i++) {
      fix.push(i);
    }
  }

  return fix;
}

console.log(range(1, 10)); //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)); // -1
console.log(range(11, 18)); // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)); // [54, 53, 52, 51, 50]
console.log(range()); // -1

console.log("Soal No 2\n");

function rangeWithStep(startNum, finishNum, step) {
  var fix = [];

  if (startNum > finishNum) {
    for (let i = startNum; i >= finishNum; i -= step) {
      fix.push(i);
    }
  } else {
    for (let i = startNum; i <= finishNum; i += step) {
      fix.push(i);
    }
  }
  return fix;
}

console.log(rangeWithStep(1, 10, 2)); // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)); // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)); // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)); // [29, 25, 21, 17, 13, 9, 5]

console.log("Soal No 3\n");

function sum(awalDeret, akhirDeret, step) {
  var fix = [];

  if (!akhirDeret || !awalDeret) {
    fix = 1;
    if (!akhirDeret && !step && !awalDeret) {
      fix = 0;
    }
  } else if (awalDeret > akhirDeret && !step) {
    for (let i = awalDeret; i >= akhirDeret; i--) {
      fix.push(i);
    }
  } else if (awalDeret > akhirDeret) {
    for (let i = awalDeret; i >= akhirDeret; i -= step) {
      fix.push(i);
    }
  } else if (!step) {
    for (let i = awalDeret; i <= akhirDeret; i++) {
      fix.push(i);
    }
  } else if (awalDeret < akhirDeret) {
    for (let i = awalDeret; i <= akhirDeret; i += step) {
      fix.push(i);
    }
  } else {
    fix = 0;
  }

  var total = 0;
  for (var i = 0; i < fix.length; ++i) {
    total += fix[i];
  }
  if (fix == 1) {
    total = 1;
  }
  return total;
}

console.log(sum(1, 10)); // 55
console.log(sum(5, 50, 2)); // 621
console.log(sum(15, 10)); // 75
console.log(sum(20, 10, 2)); // 90
console.log(sum(1)); // 1
console.log(sum()); // 0

console.log("Soal No 4\n");

var input = [
  ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
  ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
  ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
  ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"],
];

var biodata = ["NO", "NAMA", "KOTA", "TTL", "HOBBY"];

function dataHandling(input) {
  var s = "";
  for (let i = 0; i < input.length; i++) {
    s +=
      "\n" +
      "Nomor ID" +
      " : " +
      input[i][0] +
      "\n" +
      "Nama Lengkap" +
      " : " +
      input[i][1] +
      "\n" +
      "TTL" +
      " : " +
      input[i][2] +
      " " +
      input[i][3] +
      "\n" +
      "Hobby" +
      " : " +
      input[i][4] +
      "\n";
  }
  return s;
}
console.log(dataHandling(input));

console.log("Soal No 5");
function balikKata(kalimat) {
  var firstString = kalimat;
  var newString = "";

  for (let i = kalimat.length - 1; i >= 0; i--) {
    newString = newString + firstString[i];
  }

  return newString;
}

console.log(balikKata("Kasur Rusak")); // kasuR rusaK
console.log(balikKata("SanberCode")); // edoCrebnaS
console.log(balikKata("Haji Ijah")); // hajI ijaH
console.log(balikKata("racecar")); // racecar
console.log(balikKata("I am Sanbers")); // srebnaS ma I

console.log("\nSoal No 6\n");

var string = [
  "0001",
  "Roman Alamsyah Elsharawy",
  "Bandar Lampung",
  "21/05/1989",
  "Membaca",
];
function dataHandling2(string) {
  string.splice(4, 1, "Pria", "SMA Internasional Metro");
  console.log(string);
  var bulansplit = string[3].split("/");
  var bulan = "";
  switch (bulansplit[1]) {
    case "01": {
      bulan = "Januari";
      break;
    }
    case "02": {
      bulan = "Februari";
      break;
    }
    case "03": {
      bulan = "Maret";
      break;
    }
    case "04": {
      bulan = "April";
      break;
    }
    case "05": {
      bulan = "Mei";
      break;
    }
    case "06": {
      bulan = "Juni";
      break;
    }
    case "07": {
      bulan = "Juli";
      break;
    }
    case "08": {
      bulan = "Agustus";
      break;
    }
    case "09": {
      bulan = "September";
      break;
    }
    case "10": {
      bulan = "Oktober";
      break;
    }
    case "11": {
      bulan = "November";
      break;
    }
    case "12": {
      bulan = "Desember";
      break;
    }
    default: {
      console.log("Salah Input");
    }
  }
  console.log(bulan);
  var strip = bulansplit.join("-");
  bulansplit.sort(function (a, b) {
    return b - a;
  });
  console.log(bulansplit);

  console.log(strip);
  var nama = string[1].slice(0, 15);
  console.log(nama);
}
// var name = string.split()

dataHandling2(string);
