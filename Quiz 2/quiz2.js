class Score {
  constructor(subject, points, email) {
    this.subject = subject;
    this.points = points;
    this.email = email;
  }

  average() {
    const rata = (array) => {
      var total = 0;
      var rata = 0;
      for (let i = 0; i < array.length; i++) {
        total += array[i];
      }
      rata = total / array.length;
      return rata;
    };
    var arrat = this.points;
    if (arrat.length > 0) {
      return rata(this.points);
    } else {
      return this.points;
    }
  }
}

var rata2 = new Score(
  "menghitung nilai rata2 dari array",
  [25, 28, 29, 40],
  "mohammad.rizqypratama@gmail.com"
);
console.log(rata2.average());

const data = [
  ["email", "quiz-1", "quiz-2", "quiz-3"],
  ["abduh@mail.com", 78, 89, 90],
  ["khairun@mail.com", 95, 85, 88],
  ["bondra@mail.com", 70, 75, 78],
  ["regi@mail.com", 91, 89, 93],
];

const viewScores = (data, subject) => {
  var hasil = [];

  for (let i = 0; i < data.length; i++) {
    if (data[0][i] == subject) {
      for (let j = 1; j < data.length; j++) {
        var nilai = new Score(subject, data[j][i], data[j][0]);
        var tampung = {};
        tampung.email = nilai.email;
        tampung.subject = nilai.subject;
        tampung.points = nilai.points;
        hasil.push(tampung);
      }
    }
  }
  console.log(hasil);
};

// TEST CASE
viewScores(data, "quiz-1");
viewScores(data, "quiz-2");
viewScores(data, "quiz-3");

const recapScores = (data) => {
  for (var i = 1; i < data.length; i++) {
    var isinilai = [];
    for (var j = 1; j < data[i].length; j++) {
      isinilai.push(parseInt(data[i][j]));
    }

    var nilai = new Score("", isinilai, "");
    console.log(i + ". Email: " + data[i][0]);
    console.log("Rata-rata: " + nilai.average());
    if (nilai.average() > 90) {
      console.log("Predikat: honour");
    } else if (nilai.average() > 80 && nilai.average() <= 90) {
      console.log("Predikat: graduate");
    } else if (nilai.average() > 70 && nilai.average() <= 80) {
      console.log("Predikat: participant");
    }
    console.log();
  }
};

recapScores(data);
