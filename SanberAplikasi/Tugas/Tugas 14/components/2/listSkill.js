import React, { Component } from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome5";
import Icon2 from "react-native-vector-icons/MaterialIcons";

export default class listSkill extends Component {
  render() {
    let skill = this.props.skill;

    return (
      <View style={styles.container}>
        <View style={styles.containerSkill}>
          <View style={styles.imageSkill}>
            <Image
              source={{ uri: skill.logoUrl }}
              style={{ height: 70, width: 70 }}
            />
          </View>
          <View style={styles.infoText}>
            <Text style={styles.textName}>{skill.skillName}</Text>
            <Text>{skill.categoryName}</Text>
            <Text style={styles.textPercen}>{skill.percentageProgress}</Text>
          </View>
          <View style={styles.navSkill}>
            <Icon2 name="navigate-next" size={70} />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    padding: 15,
  },
  containerSkill: {
    flexDirection: "row",
    justifyContent: "space-around",
    height: 110,
    width: 320,
    // padding: 50,
    backgroundColor: "#FF9800",
    borderRadius: 16,
  },
  infoText: {
    justifyContent: "center",
    // alignItems: "center",
  },
  navSkill: {
    justifyContent: "center",
  },
  imageSkill: {
    justifyContent: "center",
  },
  textPercen: {
    fontSize: 40,
    fontWeight: "bold",
    color: "white",
    alignSelf: "flex-end",
  },
  textName: {
    fontSize: 16,
    alignSelf: "center",
    fontWeight: "600",
  },
});
