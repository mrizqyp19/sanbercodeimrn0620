import React from "react";
import {
  View,
  Text,
  ScrollView,
  FlatList,
  TouchableOpacity,
  StyleSheet,
  Image,
} from "react-native";
import Icon from "react-native-vector-icons/FontAwesome5";
import dataSkill from "./skillData.json";
import ListSkill from "./listSkill";

export default class Skill extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.bgContainer}>
          <View style={styles.logo}>
            <Image
              source={require("./kinyota.png")}
              style={{ width: 90, height: 70 }}
            />
          </View>
          <View style={styles.infoUser}>
            <Image source={require("./dummy.jpg")} style={styles.avatar} />
            <View style={styles.namaUser}>
              <Text>Hai,</Text>
              <Text>Mohammad Rizqy Pratama</Text>
            </View>
          </View>

          <Text style={{ fontSize: 30, fontWeight: "bold", margin: 35 }}>
            SKILL
          </Text>
          <View
            style={{
              height: 4,
              backgroundColor: "black",
              marginLeft: 15,
              marginRight: 20,
            }}
          />
          <View style={styles.category}>
            <View style={styles.containerCategory}>
              <Text>Library/Framework</Text>
            </View>
            <View style={styles.containerCategory1}>
              <Text>Bahasa Pemograman</Text>
            </View>
            <View style={styles.containerCategory2}>
              <Text>Teknologi</Text>
            </View>
          </View>
        </View>
        <View style={styles.body}>
          <FlatList
            data={dataSkill.items}
            renderItem={(skill) => <ListSkill skill={skill.item} />}
            keyExtractor={(item) => item.id}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  body: {
    paddingTop: 30,
    paddingBottom: 20,
    flex: 1,
  },
  bgContainer: {
    height: 330,
    backgroundColor: "#6075BC",
    borderRadius: 16,
    paddingTop: 40,
    paddingBottom: 40,
    // flex: 1,
  },
  logo: {
    flexDirection: "row",
    justifyContent: "flex-end",
  },
  infoUser: {
    flexDirection: "row",
    alignItems: "center",
    paddingLeft: 30,
  },
  namaUser: {
    paddingLeft: 20,
    justifyContent: "center",
  },
  avatar: {
    height: 60,
    width: 60,
    borderRadius: 50,
    backgroundColor: "#fff",
  },
  category: {
    flexDirection: "row",
    justifyContent: "space-around",

    flexWrap: "wrap",
  },
  containerCategory: {
    height: 30,
    width: 130,
    backgroundColor: "#FF9800",
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center",
    margin: 10,
  },
  containerCategory1: {
    height: 30,
    width: 150,
    backgroundColor: "#FF9800",
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center",
    margin: 10,
  },
  containerCategory2: {
    height: 30,
    width: 130,
    backgroundColor: "#FF9800",
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center",
    // margin: 10,
  },
});
