import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  FlatList,
} from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";

export default class Register extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.bgContainer}>
          <Image source={require("./kinyot.png")} style={styles.logo} />
          <View style={styles.formLogin}>
            <View style={styles.formm}>
              <Text style={styles.text}>Username</Text>
              <View style={styles.box}></View>
            </View>
            <View style={styles.formm}>
              <Text style={styles.text}>Email</Text>
              <View style={styles.box}></View>
            </View>
            <View style={styles.formm}>
              <Text style={styles.text}>Password</Text>
              <View style={styles.box}></View>
            </View>
            <View style={styles.formm}>
              <Text style={styles.text}>Confirm Password</Text>
              <View style={styles.box}></View>
            </View>
            <View style={{ alignItems: "flex-end", padding: 25 }}>
              <View style={styles.buttonSignin}>
                <Text
                  style={{ fontSize: 16, fontWeight: "600", color: "white" }}
                >
                  Sign Up
                </Text>
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  bgContainer: {
    height: 330,
    backgroundColor: "#6075BC",
    borderRadius: 16,
    alignItems: "center",
    paddingTop: 80,
  },
  logo: {
    height: 120,
    width: 82,
  },
  formLogin: {
    height: 470,
    width: 316,
    backgroundColor: "#E8E8E8",
    overflow: "hidden",
    borderRadius: 16,
    margin: 30,
  },
  formm: {
    paddingLeft: 25,
    paddingTop: 20,
  },
  box: {
    borderRadius: 8,
    width: 263,
    height: 43,
    backgroundColor: "#fff",
  },
  text: {
    fontSize: 16,
    fontWeight: "600",
    paddingBottom: 8,
  },
  buttonSignin: {
    width: 149,
    height: 38,
    backgroundColor: "#6075BC",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 8,
  },
});
