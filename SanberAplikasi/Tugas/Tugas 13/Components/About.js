import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  FlatList,
} from "react-native";
import Icon from "react-native-vector-icons/AntDesign";

export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.bgContainer}>
          <View style={styles.containerInformation}>
            <View>
              <Text
                style={{ fontSize: 18, fontWeight: "bold", paddingBottom: 10 }}
              >
                Mohammad Rizqy Pratama
              </Text>
              <Text>Bandung, Indonesia</Text>
            </View>
            <View style={styles.avatar}>
              <Image
                source={require("./we.jpeg")}
                style={{
                  height: 100,
                  width: 100,
                  borderRadius: 50,
                  backgroundColor: "#fff",
                }}
              />
            </View>
          </View>

          <View style={styles.containerSkill}>
            <Text style={{ fontWeight: "400", fontSize: 18, padding: 15 }}>
              Portofolio
            </Text>
            <View
              style={{
                height: 0.5,
                backgroundColor: "black",
                marginLeft: 15,
                marginRight: 20,
              }}
            />
            <View style={styles.containerPortofolio}>
              <View style={{ alignItems: "center" }}>
                <Icon name="gitlab" size={40} />
                <Text>@mrizqyp19</Text>
              </View>
              <View style={{ alignItems: "center" }}>
                <Icon name="github" size={40} />
                <Text>@mrizqyp19</Text>
              </View>
            </View>
          </View>
          <View style={styles.containerKontak}>
            <Text style={{ fontWeight: "400", fontSize: 18, padding: 15 }}>
              Hubungi Saya
            </Text>
            <View
              style={{
                height: 0.5,
                backgroundColor: "black",
                marginLeft: 15,
                marginRight: 20,
              }}
            />
            <View style={styles.containerWe}>
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  paddingBottom: 20,
                  paddingTop: 10,
                }}
              >
                <Icon name="facebook-square" size={40} />
                <Text>@biargampangajj</Text>
              </View>
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  paddingBottom: 20,
                  paddingTop: 10,
                }}
              >
                <Icon name="twitter" size={40} />
                <Text>@mohammadrizqy9</Text>
              </View>
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  paddingBottom: 20,
                  paddingTop: 10,
                }}
              >
                <Icon name="instagram" size={40} />
                <Text>@mohammadrizqypratama</Text>
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  bgContainer: {
    height: 330,
    backgroundColor: "#6075BC",
    borderRadius: 16,
    paddingTop: 80,
  },
  containerInformation: {
    flexDirection: "row",
    justifyContent: "space-around",
  },
  avatar: {
    height: 100,
    width: 100,
    borderRadius: 50,
    backgroundColor: "#fff",
  },
  containerSkill: {
    height: 200,
    width: 316,
    backgroundColor: "#E8E8E8",
    overflow: "hidden",
    borderRadius: 16,
    margin: 30,
  },
  containerPortofolio: {
    flexDirection: "row",
    justifyContent: "space-around",
    margin: 20,
    alignItems: "center",
  },
  containerKontak: {
    height: 300,
    width: 316,
    backgroundColor: "#E8E8E8",
    overflow: "hidden",
    borderRadius: 16,
    marginLeft: 30,
  },
  containerWe: {
    // flexDirection: "column",
    justifyContent: "space-around",
    alignItems: "center",
  },
});
